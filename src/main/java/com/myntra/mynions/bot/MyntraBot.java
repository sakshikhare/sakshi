package com.myntra.mynions.bot;

import com.myntra.mynions.bot.helper.Parameters;
import me.ramswaroop.jbot.core.slack.Bot;
import me.ramswaroop.jbot.core.slack.Controller;
import me.ramswaroop.jbot.core.slack.EventType;
import me.ramswaroop.jbot.core.slack.models.Event;
import org.springframework.web.socket.WebSocketSession;

import java.util.List;

/**
 * Created by Vishwa on 06/03/18.
 */
public abstract class MyntraBot extends Bot implements IMyntraBot {
    public abstract void onDirectMention(WebSocketSession session, Event event);
    public abstract void onDirectMessage(WebSocketSession session, Event event);

    public List<String> getCommandTokens(Event event) {
        List<String> params = null;

        if(event.getType().equals("DIRECT_MESSAGE"))
            params = Parameters.parse(event.getText(), EventType.DIRECT_MESSAGE);

        else if(event.getType().equals("DIRECT_MENTION"))
            params = Parameters.parse(event.getText(), EventType.DIRECT_MENTION);

        return params;
    }
}