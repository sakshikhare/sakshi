package com.myntra.mynions.bot.helper;

import com.jayway.jsonpath.JsonPath;
import com.myntra.mynions.http.Http;
import me.ramswaroop.jbot.core.slack.models.Event;
import net.minidev.json.JSONObject;

/**
 * Created by Vishwa on 05/03/18.
 */
public class SlackHelper {
    public static String getSlackUsername(Event event) {
        try {
            String slackUserGetUri = "https://slack.com/api/users.info?token=xoxp-2151807574-139774958706-160711240517-d52aef3350c62c1d220e432ab4e0277e&user=";
            String slackUserResponse = Http.sendGet(slackUserGetUri + event.getUserId());
            return JsonPath.read(slackUserResponse, "$.user.name");
        }
        catch (Exception ex) {
            return null;
        }
    }

    public static void sendSlackFile(String slackToken, String channel, String fileName, String content) {
        try {
            new Meteoroid.Builder()
                    .token(slackToken)
                    .fileContent(content)
                    .channels(channel)
                    .title(fileName)
                    .build()
                    .post();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
