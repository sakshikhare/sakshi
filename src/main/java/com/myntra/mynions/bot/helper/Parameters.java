package com.myntra.mynions.bot.helper;

import me.ramswaroop.jbot.core.slack.EventType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Vishwa on 05/03/18.
 */
public class Parameters {
    public static List<String> parse(String message, EventType eventType) {
        List<String> params = new LinkedList<String>(Arrays.asList(message.split("\\s+")));

        if(eventType==EventType.DIRECT_MESSAGE) {
            return params;
        }
        else if(eventType == EventType.DIRECT_MENTION) {
            params.remove(0);
        }

        return params;
    }
}
