package com.myntra.mynions.bot.helper;

import com.myntra.mynions.bot.Command;
import org.drools.compiler.compiler.DroolsParserException;
import org.drools.compiler.compiler.PackageBuilder;
import org.drools.core.RuleBase;
import org.drools.core.RuleBaseFactory;
import org.drools.core.WorkingMemory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

/**
 * Created by Vishwa on 06/03/18.
 */
public class DroolsHelper {
    public static Command executeRule(Command command, String ruleFileName) {
        try {
            PackageBuilder packageBuilder = new PackageBuilder();

            String ruleFile = ruleFileName;
            InputStream resourceAsStream = DroolsHelper.class.getResourceAsStream(ruleFile);

            Reader reader = new InputStreamReader(resourceAsStream);
            packageBuilder.addPackageFromDrl(reader);
            org.drools.core.rule.Package rulesPackage = packageBuilder.getPackage();
            RuleBase ruleBase = RuleBaseFactory.newRuleBase();
            ruleBase.addPackage(rulesPackage);

            WorkingMemory workingMemory = ruleBase.newStatefulSession();

            workingMemory.insert(command);
            workingMemory.fireAllRules();
        }
        catch (IOException | DroolsParserException ex) {
            ex.printStackTrace();
            command.setResponse(null);
        }

        return command;
    }
}
