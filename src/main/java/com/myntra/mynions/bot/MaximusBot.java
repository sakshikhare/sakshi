package com.myntra.mynions.bot;

import com.myntra.mynions.bot.command.search.maximus.MaximusClient;
import com.myntra.mynions.bot.helper.DroolsHelper;
import com.myntra.mynions.bot.helper.SlackHelper;
import me.ramswaroop.jbot.core.slack.Bot;
import me.ramswaroop.jbot.core.slack.Controller;
import me.ramswaroop.jbot.core.slack.EventType;
import me.ramswaroop.jbot.core.slack.models.Event;
import me.ramswaroop.jbot.core.slack.models.Message;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

import java.util.List;

/**
 * Created by Vishwa on 06/03/18.
 */
@Component
public class MaximusBot extends MyntraBot {
    @Override
    public String getName() {
        return "maximus";
    }

    @Override
    @Controller(events = {EventType.DIRECT_MENTION})
    public void onDirectMention(WebSocketSession session, Event event) {
        processRequest(session,event);
    }

    @Override
    @Controller(events = {EventType.DIRECT_MESSAGE})
    public void onDirectMessage(WebSocketSession session, Event event) {
        processRequest(session,event);
    }

    private void processRequest(WebSocketSession session, Event event) {
        try {
            /*Command c = new Command();
            c.setUser(SlackHelper.getSlackUsername(event));
            c.setNamespace(getName());
            c.setParameters(getCommandTokens(event));
            DroolsHelper.executeRule(c, "/com/rule/maximus.drl");
            reply(session, event, new Message(c.getResponse()));*/
            try {
                List<String> params = getCommandTokens(event);
                String message = event.getText();

                //Added this to ignore a mention after uploading file to Slack
                if(message.contains("uploaded a file"))
                    return;

                if(params.size()==0) {
                    reply(session, event, new Message("I didn't understand your request. Try 'help'"));
                    return;
                }

                if(params.get(0).equalsIgnoreCase("help")) {
                    StringBuilder builder = new StringBuilder();
                    builder.append("*Maximus bot*\n");
                    builder.append("Get maximus response for style on Slack.\n\n");
                    builder.append("*Commands:*\n");
                    builder.append("- *style_id* or *style_id* *all*  Get all information for style_id\n");
                    builder.append("- *style_id* *cms*  Get only Catalog data for style_id\n");
                    builder.append("- *style_id* *discount*  Get only Discount data for style_id\n");
                    reply(session, event, new Message(builder.toString()));
                    return;
                }

                Long styleId = Long.parseLong(params.get(0));
                String field = "all";
                if(params.size()>=2) {
                    field = params.get(1);
                }

                String response = MaximusClient.getStyleData(styleId, field);

                if(response.length() < 3994)
                    reply(session, event, new Message("```"+response+"```"));
                else
                    SlackHelper.sendSlackFile(getSlackToken(), event.getChannelId(), styleId+"-maximus-style-response-"+field, response);
            }
            catch (Exception ex) {
                reply(session, event, new Message("I didn't understand your request. Try 'help'"));
            }
        }
        catch (Exception ex) {
            reply(session, event, new Message("> Sorry! I did not understand this request. Try `help`"));
        }
    }

    @Override
    public String getSlackToken() {
        //return "xoxb-325992481366-gyr90mP5Aa5tRUi1zSTKyYmF";
        return "xoxb-231953322641-BcB5XfK1SJ3fqlk9G0eF6jVy";
    }

    @Override
    public Bot getSlackBot() {
        return this;
    }
}
