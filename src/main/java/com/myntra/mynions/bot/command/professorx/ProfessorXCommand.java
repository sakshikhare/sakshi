package com.myntra.mynions.bot.command.professorx;

import com.jayway.jsonpath.JsonPath;
import com.myntra.mynions.http.Http;
import net.minidev.json.JSONArray;
import org.springframework.beans.factory.annotation.Value;

import java.net.URLEncoder;

/**
 * Created by Vishwa on 06/03/18.
 */
public class ProfessorXCommand {
    private static String SEARCH_URL="http://developer.myntra.com/v2/search/data/";

    public static String search(String query) {
        StringBuilder response = new StringBuilder();
        try {
            return processSearchRequest(query);
        }
        catch (Exception ex) {
            response.append("I know everything, but with some exceptions like this request! Use `help`?");
        }

        return null;
    }

    public static String help() {
        String response = "> `search` <Search Query of your choice>";
        return response;
    }

    private static String processSearchRequest(String query) {

        StringBuilder response = new StringBuilder();

        try {
            String searchResponse = Http.sendGet(SEARCH_URL + URLEncoder.encode(query.trim().replaceAll(" ","-")));
            int totalProductCount = JsonPath.read(searchResponse, "$.data.totalProductsCount");
            response.append("> Products found: `" + totalProductCount + "`\n");

            String resolvedQuery = JsonPath.read(searchResponse, "$.data.search.query");
            response.append("> Resolved Query: `" + resolvedQuery + "`\n");

            String queryType = JsonPath.read(searchResponse, "$.data.queryType[0]");
            if (queryType.equalsIgnoreCase("LANDINGPAGE"))
                response.append("> This is a `Landing Page`\n");
            else
                response.append("> This is Not a Landing Page\n");

            try {
                int nbrCount = JsonPath.read(searchResponse, "$.data.nextBestResults.length()");
                if (nbrCount > 0)
                    response.append("> NBR available for this query\n");
            }
            catch (Exception ex) {
                response.append("> NBR Not available for this query\n");
            }

            try {
                JSONArray changelogs = JsonPath.read(searchResponse, "$.data.search.changeLog");

                if( changelogs.size()>0 )
                    response.append("> Changelog: \n>```");

                for(int i=0;i<changelogs.size();i++){
                    response.append( changelogs.get(i) + "\n");
                }

                if( changelogs.size()>0 )
                    response.append("```");
            }
            catch (Exception ex) {
                // Do nothing
                ex.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return response.toString();
    }
}
