package com.myntra.mynions.bot.command.search.maximus;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myntra.maximus.Fields;
import com.myntra.maximus.PdpResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vishwa on 29/08/17.
 */
public class MaximusClient {
    public static String getStyleData(Long styleId, String field) {
        try {
            boolean onProd = false;

            String host = "maximus.myntra.com";

            List<Long> styleList = new ArrayList<Long>();

            styleList.add( styleId );

            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            PdpResponse resp;
            String response;

            com.myntra.maximus.client.MaximusClient maximusClient = new com.myntra.maximus.client.MaximusClient(host, Integer.parseInt("80"), 30000, 30000);

            Fields f = Fields.ALL;

            if(field.equalsIgnoreCase("cms"))
                f = Fields.CMS_INFO;
            else if(field.equalsIgnoreCase("discount"))
                f = Fields.DISCOUNT_INFO;
            else if(field.equalsIgnoreCase("inventory"))
                f = Fields.INV_INFO;

            resp = maximusClient.getClient("MYNTRA").getPdpData(styleList.get(0), f);
            response = gson.toJson(resp.getData().get(0));

            return response;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "NO RESPONSE";
    }
}
