package com.myntra.mynions.bot;

import me.ramswaroop.jbot.core.slack.Bot;

/**
 * Created by Vishwa on 06/03/18.
 */
public interface IMyntraBot {
    public String getName();
    public String getSlackToken();
    public Bot getSlackBot();
}
