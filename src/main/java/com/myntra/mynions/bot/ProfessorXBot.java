package com.myntra.mynions.bot;

import com.myntra.mynions.bot.helper.DroolsHelper;
import com.myntra.mynions.bot.helper.SlackHelper;
import me.ramswaroop.jbot.core.slack.Bot;
import me.ramswaroop.jbot.core.slack.Controller;
import me.ramswaroop.jbot.core.slack.EventType;
import me.ramswaroop.jbot.core.slack.models.Event;
import me.ramswaroop.jbot.core.slack.models.Message;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

/**
 * Created by Vishwa on 06/03/18.
 */
@Component
public class ProfessorXBot extends MyntraBot {
    @Override
    public String getName() {
        return "search";
    }

    @Override
    @Controller(events = {EventType.DIRECT_MENTION})
    public void onDirectMention(WebSocketSession session, Event event) {
        processRequest(session, event);
    }

    @Override
    @Controller(events = {EventType.DIRECT_MESSAGE})
    public void onDirectMessage(WebSocketSession session, Event event) {
        processRequest(session, event);
    }

    @Override
    public String getSlackToken() {
        return "xoxb-306918940951-crjKgKpLvqo3Z2rVsDXrCHnr";
    }

    @Override
    public Bot getSlackBot() {
        return this;
    }

    private void processRequest(WebSocketSession session, Event event) {
        try {
            Command c = new Command();
            //c.setUser(SlackHelper.getSlackUsername(event));
            c.setNamespace(getName());
            c.setParameters(getCommandTokens(event));
            DroolsHelper.executeRule(c, "/com/rule/search.drl");
            reply(session, event, new Message(c.getResponse()));
        }
        catch (Exception ex) {
            reply(session, event, new Message("> Sorry! I did not understand this request. Try `help`"));
        }
    }
}
