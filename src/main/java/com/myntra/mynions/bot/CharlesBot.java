package com.myntra.mynions.bot;

import com.myntra.mynions.bot.helper.DroolsHelper;
import me.ramswaroop.jbot.core.slack.Bot;
import me.ramswaroop.jbot.core.slack.Controller;
import me.ramswaroop.jbot.core.slack.EventType;
import me.ramswaroop.jbot.core.slack.models.Event;
import me.ramswaroop.jbot.core.slack.models.Message;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

/**
 * Created by Vishwa on 03/03/18.
 */
@Component
public class CharlesBot extends MyntraBot {
    @Override
    public String getName() {
        return "charles";
    }

    @Override
    public String getSlackToken() {
        return "xoxb-246854727362-w9In4LxVxtEUDXyzXX5LLbFW";
    }

    @Override
    public Bot getSlackBot() {
        return this;
    }

    @Override
    @Controller(events = {EventType.DIRECT_MENTION})
    public void onDirectMention(WebSocketSession session, Event event) {
        Command c = new Command();
        c.setNamespace( getName() );
        c.setParameters( getCommandTokens(event) );
        DroolsHelper.executeRule(c, "/com/rule/charles.drl");
        reply(session, event, new Message( c.getResponse() ));
    }

    @Override
    @Controller(events = {EventType.DIRECT_MESSAGE})
    public void onDirectMessage(WebSocketSession session, Event event) {
        Command c = new Command();
        c.setNamespace( getName() );
        c.setParameters( getCommandTokens(event) );
        DroolsHelper.executeRule(c, "/com/rule/charles.drl");
        reply(session, event, new Message( c.getResponse() ));
    }
}
