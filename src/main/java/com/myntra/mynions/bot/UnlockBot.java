package com.myntra.mynions.bot;

import com.myntra.commons.auth.ContextInfo;
import com.myntra.commons.client.exception.WebClientException;
import com.myntra.coupon.client.CouponManagerServiceClient;
import com.myntra.coupon.response.GenericCouponAPIResponse;
import com.myntra.coupon.response.LockUnlockUserCouponResponse;
import com.myntra.coupon.response.entry.CouponValidEntry;
import com.myntra.coupon.response.entry.LockUnlockUserCouponEntry;
import me.ramswaroop.jbot.core.slack.Bot;
import me.ramswaroop.jbot.core.slack.Controller;
import me.ramswaroop.jbot.core.slack.models.Event;
import me.ramswaroop.jbot.core.slack.models.Message;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;


@Component
public class UnlockBot extends Bot {

    String coupon;
    String userid;
    String header;
    static String serviceUrl = "http://pp-coupon.myntra.com";

    @Controller(pattern = "(unlock)(?i)", next = "isValid")
    public void unlockMeFast(WebSocketSession session, Event event) throws WebClientException {

        isValid(session, event);


    }


    @Controller(next = "checkAnswer")
    public void isValid(WebSocketSession session, Event event) throws WebClientException {
        try {

            try {

                coupon = event.getText().replaceAll("unlock ", "").split(",")[0];
                userid = event.getText().replaceAll("unlock ", "").split(",")[1];
                header = event.getText().replaceAll("unlock ", "").split(",")[2];

                GenericCouponAPIResponse response = subscribeAppToPage(event, coupon, userid, header);


                reply(session, event, new Message("*`Response to this coupon : `* _" + response.getStatus().getStatusMessage() + "_"));
                if (response.getStatus().getStatusMessage().equalsIgnoreCase("Coupon is locked.")) {
                    reply(session, event, new Message("_Unlocking coupon.._ : *" + coupon + "*"));

                    unlockCouponEvent(session, event);
                } else {
                    reply(session, event, new Message("_Coupon not eligible for unlocking._"));
                    reply(session, event, new Message("_Bye!_"));


                }

/*                    reply(session, event, new Message("Type *Yes* to continue or *No* to abort"));
                   nextConversation(event);*/

                stopConversation(event);

            } catch (Exception ex) {
                reply(session, event, new Message("I didn't understand your request. Try '*unlock me*'"));
            }
        } catch (Exception ex) {
            reply(session, event, new Message("> Sorry! I did not understand this request. Try `help`"));
        }


    }
   /* @Controller(pattern = "^(?i)(Yes|No)$",next = "unlockCouponEvent")
    public void checkAnswer(WebSocketSession session, Event event) throws WebClientException {


        if(event.getText().contains("Yes")) {
            reply(session, event, new Message("*Unlock Response* : "+unlockCoupon(coupon,userid,header)));

        }
        else {
            reply(session, event, new Message("*Okay Bye!*"));
            stopConversation(event);


        }


    }*/


    @Controller()
    public void unlockCouponEvent(WebSocketSession session, Event event) throws WebClientException {


        coupon = event.getText().replaceAll("unlock ", "").split(",")[0];
        userid = event.getText().replaceAll("unlock ", "").split(",")[1];
        header = event.getText().replaceAll("unlock ", "").split(",")[2];


        String unlockResponse = unlockCoupon(coupon, userid, header);
        reply(session, event, new Message("*`Unlock Coupon Response :`*    _" + unlockResponse + "_"));
        reply(session, event, new Message("_Bye!_"));


    }


    public static GenericCouponAPIResponse subscribeAppToPage(Event event, String coupon, String userid, String header) throws WebClientException {
        String message = event.getText();
        CouponManagerServiceClient client = new CouponManagerServiceClient();
        ContextInfo contextInfo = new ContextInfo();
        CouponValidEntry entry = new CouponValidEntry();
        entry.setCoupon(coupon);
        entry.setLogin(userid);
        entry.setChannel("online");
        entry.setDevice("");
        entry.setValidateGroup(false);
        if (header.equalsIgnoreCase("Myntra")) {
            header = "storeid=2297;";
        } else if (header.equalsIgnoreCase("Jabong")) {
            header = "storeid=4603;";

        }
        GenericCouponAPIResponse response = client.isCouponValidForUser(entry, serviceUrl, contextInfo, 1000, 1, header);
        return response;
    }

    public static String unlockCoupon(String coupon, String userid, String header) throws WebClientException {
        CouponManagerServiceClient client = new CouponManagerServiceClient();
        ContextInfo contextInfo = new ContextInfo();
        LockUnlockUserCouponEntry entry = new LockUnlockUserCouponEntry();
        entry.setCoupon(coupon);
        entry.setUserId(userid);
        if (header.equalsIgnoreCase("Myntra")) {
            header = "storeid=2297;";
        } else if (header.equalsIgnoreCase("Jabong")) {
            header = "storeid=4603;";

        }
        LockUnlockUserCouponResponse response = client.unlockCouponForUser(entry, serviceUrl, contextInfo, 1000, 1, header);
        return response.getStatus().getStatusMessage();
    }

    @Override
    public String getSlackToken() {
        return "xoxb-2151807574-384739818167-D8g4N8KIKv8cIE0ac6AqHMVe";
    }

    @Override
    public Bot getSlackBot() {
        return this;
    }
}

