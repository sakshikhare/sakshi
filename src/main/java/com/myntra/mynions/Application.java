package com.myntra.mynions;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by Vishwa on 22/02/18.
 */
/*@SpringBootApplication(scanBasePackages = {
        "me.ramswaroop.jbot",
        "com.myntra.mynions"
})*/
@SpringBootApplication(
    scanBasePackages = {
            "me.ramswaroop.jbot",
    },
    scanBasePackageClasses = {
            com.myntra.mynions.bot.UnlockBot.class
})
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
