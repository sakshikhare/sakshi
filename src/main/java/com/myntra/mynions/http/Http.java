package com.myntra.mynions.http;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.zip.GZIPInputStream;

/**
 * Created by Vishwa on 05/03/18.
 */
public class Http {
    public static String sendGet(String url) throws Exception {

        try {
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("GET");


            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            return response.toString();
        } catch (IOException e) {
            //e.printStackTrace();
        }

        return null;
    }

    private static String sendPost(String url, String jsonData) throws Exception {
        HashMap<String, String> headerList = new HashMap<String, String>();
        headerList.put("Accept", "application/json");
        headerList.put("Content-Type", "application/json");

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod("POST");

        Iterator<HashMap.Entry<String, String>> it = headerList.entrySet().iterator();
        while (it.hasNext()) {
            HashMap.Entry<String, String> pair = it.next();
            con.setRequestProperty(pair.getKey(), pair.getValue());
        }

        con.setDoOutput(true);
        OutputStream os = con.getOutputStream();
        os.write(jsonData.getBytes("UTF-8"));
        os.close();

        int responseCode = con.getResponseCode();

        Reader reader = null;
        if ("gzip".equals(con.getContentEncoding())) {
            reader = new InputStreamReader(new GZIPInputStream(con.getInputStream()));
        }
        else {
            reader = new InputStreamReader(con.getInputStream());
        }

        StringBuffer response = new StringBuffer();
        while (true) {
            int ch = reader.read();
            if (ch==-1) {
                break;
            }
            response.append((char)ch);
        }

        reader.close();

        return response.toString();
    }
}
