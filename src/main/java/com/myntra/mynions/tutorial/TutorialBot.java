package com.myntra.mynions.tutorial;

import com.myntra.mynions.bot.MyntraBot;
import me.ramswaroop.jbot.core.slack.Bot;
import me.ramswaroop.jbot.core.slack.models.Event;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

/**
 * Created by Vishwa on 06/03/18.
 */
/*
    Step 1: Create a Bot class extending MyntraBot
    Step 2: Implement following methods:
        -> getName() : This will name your bot. Use an appropriate and sensible name for the bot.
                       This can be used in .drl files where you define rules.
        -> getSlackToken(): Returns Slack bot token. To create a bot you may refer to this: https://myntra.slack.com/apps/A0F7YS25R-bots#?page=1
        -> getSlackBot(): This will return instance of the bot. "return this;" is easier!
    Step 3: onDirectMention()
 */
@Component
public class TutorialBot extends MyntraBot {
    @Override
    public String getName() {
        return "tutorial";
    }

    @Override
    public void onDirectMention(WebSocketSession session, Event event) {

    }

    @Override
    public void onDirectMessage(WebSocketSession session, Event event) {

    }

    @Override
    public String getSlackToken() {
        return null;
    }

    @Override
    public Bot getSlackBot() {
        return null;
    }
}
